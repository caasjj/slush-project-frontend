/**
 * This file contains all necessary Angular controller definitions for 'frontend.example.login' module.
 *
 * Note that this file should only contain controllers and nothing else.
 */
(function () {
    'use strict';

    /**
     * Login controller to handle user's login to application. Controller uses 'Auth' service to make actual HTTP
     * request to server and try to authenticate user.
     *
     * After successfully login Auth service will store user data and JWT token via 'Storage' service where those are
     * asked whenever needed in application.
     *
     * @todo
     *  1) different authentication providers
     *  2) user registration
     */
    angular.module( 'frontend.auth.register' )
        .controller( 'RegisterController',
        function _authRegisterRegisterController ( $scope, $state, BackendConfig, Auth ) {

            $scope.register = function () {

                Auth
                    .register( $scope.account )
                    .then(
                        function () {
                         return Auth.requestConfirmation( $scope.account )
                        },
                        function() {
                          alert('Crap!  Request confirmatoin failed!');
                          $state.go('auth.login');
                        } )
                    .then( function () {
                        $state.go( 'auth.login' )
                    } );

            }
        }
    )
        .controller( 'ConfirmationController',
        function _authRegisterConfirmationController ( $scope, $state, $location, Auth ) {

            Auth.completeConfirmation( $location.search().h )
                .then( function () {
                    $state.go( 'auth.login' );
                },
                function() {
                    alert('Crap! Confirming account failed!');
                    $state.go('auth.login');
                } );

            console.log( 'Confirmation Controller activated with hash ', $location.search().h );

        }
    );

}());
