/**
 * Messages component which is divided to following logical components:
 *
 *  Controllers
 *
 * All of these are wrapped to 'frontend.login' angular module.
 */
(function () {
    'use strict';

    // Define frontend.auth.login angular module
    angular.module( 'frontend.auth.register', [] );

    // Module configuration
    angular.module( 'frontend.auth.register' )
        .config( function _authRegister ( $stateProvider ) {

            console.log( 'Registr!!' );

            $stateProvider
                .state( 'auth.register', {
                    url  :'/register',
                    data :{
                        access:0
                    },
                    views:{
                        'content@':{
                            templateUrl:'/frontend/auth/register/register.html',
                            controller :'RegisterController'
                        }
                    }
                } )
                .state( 'auth.confirm', {
                    url  :'/confirmation',
                    data :{
                        access:0
                    },
                    views:{
                        'content@':{
                            controller:'ConfirmationController'
                        }
                    }
                } );
        }

    );
}());
